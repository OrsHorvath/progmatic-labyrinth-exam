/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.CellType;
import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.exceptions.CellException;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import com.progmatic.labyrinthproject.interfaces.Player;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class RandomPlayer implements Player {

    @Override
    public Direction nextMove(Labyrinth l) {
        
        try {
            if (l.getCellType(l.getPlayerPosition()).equals(CellType.END)) {
                return null;
            } else {
                List<Direction> pDirections = l.possibleMoves();
                double r = Math.random();
                if (pDirections.size() == 1) {
                    return pDirections.get(0);
                }
                if (pDirections.size() == 2) {
                    if (r < 0.5) {
                        return pDirections.get(0);
                    } else {
                        return pDirections.get(1);
                    }
                }
                if (pDirections.size() == 3) {
                    if (r < 0.33) {
                        return pDirections.get(0);
                    } else if (r > 0.66) {
                        return pDirections.get(1);
                    } else {
                        return pDirections.get(2);
                    }
                }
                if (pDirections.size() == 4) {
                    if (r < 0.25) {
                        return pDirections.get(0);
                    } else if (r >= 0.25 && r < 0.5) {
                        return pDirections.get(1);
                    } else if (r >= 0.5 && r < 0.75) {
                        return pDirections.get(2);
                    } else {
                        return pDirections.get(3);
                    }
                }
            }
        } catch (CellException ex) {
            Logger.getLogger(RandomPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
