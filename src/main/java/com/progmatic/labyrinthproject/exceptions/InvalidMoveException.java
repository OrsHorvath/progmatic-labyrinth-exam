package com.progmatic.labyrinthproject.exceptions;

import com.progmatic.labyrinthproject.enums.Direction;

/**
 *
 * @author pappgergely
 */
public class InvalidMoveException extends Exception {

    Direction direction;
    String message;

    public InvalidMoveException(Direction direction) {
        this.direction = direction;
        message = "nem megfelelő irány!";

    }

}
