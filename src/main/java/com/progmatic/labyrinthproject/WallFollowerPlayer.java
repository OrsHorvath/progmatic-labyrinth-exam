/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.CellType;
import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.exceptions.CellException;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import com.progmatic.labyrinthproject.interfaces.Player;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class WallFollowerPlayer implements Player {

    Direction previousStep;
    List<Direction> preferences;

    public WallFollowerPlayer(String hand) {
        preferences = new ArrayList<>();

        if (hand.equals("left")) {
            preferences.add(Direction.WEST);
            preferences.add(Direction.NORTH);
            preferences.add(Direction.EAST);
            preferences.add(Direction.SOUTH);
        } else if (hand.equals("right")) {
            preferences.add(Direction.EAST);
            preferences.add(Direction.SOUTH);
            preferences.add(Direction.WEST);
            preferences.add(Direction.NORTH);
        }
    }

    @Override
    public Direction nextMove(Labyrinth l) {
        Direction d = null;
        List<Direction> pDirections = l.possibleMoves();
        try {
            if (l.getCellType(l.getPlayerPosition()).equals(CellType.END)) {
                return null;
            }
        } catch (CellException ex) {
            Logger.getLogger(WallFollowerPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (l.possibleMoves().size() == 1) {
            d = pDirections.get(0);
            previousStep = d;
            return d;
        } else {
            if (previousStep == null) {
                for (Direction preference : preferences) {
                    if (pDirections.contains(preference)) {
                        d = preference;
                        previousStep = d;
                        return d;
                    }
                }
            } else {
                for (Direction preference : preferences) {
                    if (pDirections.contains(preference)) {
                        if (!isOpposite(preference, previousStep)) {
                            d = preference;
                            previousStep = d;
                            return d;
                        }
                        if (isOpposite(preference, previousStep) && pDirections.size() == 1) {
                            d = preference;
                            previousStep = d;
                            return d;
                        }
                    }
                }
            }
        }
        return null;
    }

    private boolean isOpposite(Direction a, Direction b) {
        if (a.equals(Direction.EAST) && b.equals(Direction.WEST)) {
            return true;
        }
        if (b.equals(Direction.EAST) && a.equals(Direction.WEST)) {
            return true;
        }
        if (a.equals(Direction.NORTH) && b.equals(Direction.SOUTH)) {
            return true;
        }
        if (a.equals(Direction.SOUTH) && b.equals(Direction.NORTH)) {
            return true;
        }
        return false;
    }
}
