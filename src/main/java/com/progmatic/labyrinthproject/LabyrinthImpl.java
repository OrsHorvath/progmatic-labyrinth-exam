package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.CellType;
import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.exceptions.CellException;
import com.progmatic.labyrinthproject.exceptions.InvalidMoveException;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pappgergely
 */
public class LabyrinthImpl implements Labyrinth {

    private int width;
    private int height;
    private CellType[][] labyrinth;
    private Coordinate playerPosition;

    public LabyrinthImpl() {
        this.height = -1;
        this.width = -1;

    }

    @Override
    public void loadLabyrinthFile(String fileName) {
        try {
            Scanner sc = new Scanner(new File(fileName));
            int width = Integer.parseInt(sc.nextLine());
            int height = Integer.parseInt(sc.nextLine());

            setSize(width, height);

            for (int hh = 0; hh < height; hh++) {
                String line = sc.nextLine();
                for (int ww = 0; ww < width; ww++) {
                    CellType type;
                    Coordinate c;
                    switch (line.charAt(ww)) {
                        case 'W':
                            type = CellType.WALL;
                            break;
                        case 'E':
                            type = CellType.END;
                            break;
                        case 'S':
                            type = CellType.START;

                            break;
                        default:
                            type = CellType.EMPTY;
                            break;
                    }
                    c = new Coordinate(ww, hh);
                    setCellType(c, type);

                }
            }
        } catch (FileNotFoundException | NumberFormatException ex) {
            System.out.println(ex.toString());
        } catch (CellException ex) {
            Logger.getLogger(LabyrinthImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public CellType getCellType(Coordinate c) throws CellException {
        if (c.getRow() < 0 || c.getRow() >= height || c.getCol() < 0 || c.getCol() >= width) {
            String message = "nincs ilyen koordináta a labirintusban";
            CellException ce = new CellException(c, message);
            throw ce;
        } else {
            return labyrinth[c.getRow()][c.getCol()];
        }
    }

    @Override
    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
        labyrinth = new CellType[this.height][this.width];

    }

    @Override
    public void setCellType(Coordinate c, CellType type) throws CellException {
        if (c.getRow() < 0 || c.getRow() >= height || c.getCol() < 0 || c.getCol() >= width) {
            String message = "nincs ilyen koordináta a labirintusban";
            CellException ce = new CellException(c, message);
            throw ce;
        } else {
            labyrinth[c.getRow()][c.getCol()] = type;
            if (type.equals(CellType.START)) {
                setPlayerPosition(c);
            }
        }
    }

    @Override
    public Coordinate getPlayerPosition() {
        return playerPosition;
    }

    public void setPlayerPosition(Coordinate p) {
        this.playerPosition = p;
    }

    @Override
    public boolean hasPlayerFinished() {
        CellType type = null;
        try {
            type = getCellType(playerPosition);
        } catch (CellException ex) {

        }
        if (type.equals(CellType.END)) {
            return true;
        }
        return false;
    }

    @Override
    public List<Direction> possibleMoves() {
        List<Direction> directions = new ArrayList<>();
        int y = playerPosition.getRow();
        int x = playerPosition.getCol();

        if (y > 0 && (labyrinth[y - 1][x] == CellType.EMPTY || labyrinth[y - 1][x] == CellType.END)) {
            directions.add(Direction.NORTH);
        }
        if (y < height - 1 && (labyrinth[y + 1][x] == CellType.EMPTY || labyrinth[y + 1][x] == CellType.END)) {
            directions.add(Direction.SOUTH);
        }
        if (x > 0 && (labyrinth[y][x - 1] == CellType.EMPTY || labyrinth[y][x - 1] == CellType.END)) {
            directions.add(Direction.WEST);
        }
        if (x < width - 1 && (labyrinth[y][x + 1] == CellType.EMPTY || labyrinth[y][x + 1] == CellType.END)) {
            directions.add(Direction.EAST);
        }
        return directions;

    }

    @Override
    public void movePlayer(Direction direction) throws InvalidMoveException {
        int y = playerPosition.getRow();
        int x = playerPosition.getCol();
        if (direction.equals(Direction.EAST)) {
            if (x + 1 > width - 1 || labyrinth[y][x + 1].equals(CellType.WALL)) {
                InvalidMoveException imv = new InvalidMoveException(direction);
                throw imv;
            } else {
                Coordinate c = new Coordinate((x + 1),y);
                setPlayerPosition(c);
            }
        }
        if (direction.equals(Direction.WEST)) {
            if (x - 1 < 0 || labyrinth[y][x - 1].equals(CellType.WALL)) {
                InvalidMoveException imv = new InvalidMoveException(direction);
                throw imv;
            } else {
                Coordinate c = new Coordinate( (x - 1),y);
                setPlayerPosition(c);
            }
        }
        if (direction.equals(Direction.SOUTH)) {
            if (y + 1 > height - 1 || labyrinth[y + 1][x].equals(CellType.WALL)) {
                InvalidMoveException imv = new InvalidMoveException(direction);
                throw imv;
            } else {
                Coordinate c = new Coordinate(x,(y + 1));
                setPlayerPosition(c);
            }
        }
        if (direction.equals(Direction.NORTH)) {
            if (y - 1 < 0 || labyrinth[y - 1][x].equals(CellType.WALL)) {
                InvalidMoveException imv = new InvalidMoveException(direction);
                throw imv;
            } else {
                Coordinate c = new Coordinate(x,(y - 1));
                setPlayerPosition(c);
            }
        }

    }

}
